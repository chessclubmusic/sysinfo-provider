
#[macro_use]
extern crate wascc_codec as codec;

#[macro_use]
extern crate log;
extern crate sys_info;
use codec::capabilities::{CapabilityProvider, Dispatcher, NullDispatcher};
use codec::core::{OP_BIND_ACTOR, OP_REMOVE_ACTOR, CapabilityConfiguration};
use codec::sysinfo;
use codec::{
    deserialize,
    serialize,
    sysinfo::*,
};

use std::error::Error;
use std::sync::RwLock;

const SYSTEM_ACTOR: &str = "system";

#[cfg(not(feature = "static_plugin"))]
capability_provider!(SysinfoProvider, SysinfoProvider::new);

const CAPABILITY_ID: &str = "wascc:sysinfo";

pub struct SysinfoProvider {
    dispatcher: RwLock<Box<dyn Dispatcher>>,
}

impl Default for SysinfoProvider {
    fn default() -> Self {
        let _ = env_logger::try_init();

        SysinfoProvider { 
            dispatcher: RwLock::new(Box::new(NullDispatcher::new())),           
        }
    }
}

impl SysinfoProvider {
    pub fn new() -> Self {
        Self::default()
    }

    fn configure(
        &self,
        _config: CapabilityConfiguration,
    ) -> Result<Vec<u8>, Box<dyn Error>> {        

        // Handle actor binding metadata here...
        // This is typically where you would establish a
        // client or connection to a resource on behalf of
        // an actor

        Ok(vec![])
    }   
    
    fn deconfigure(
        &self,
        _config: CapabilityConfiguration,        
    ) -> Result<Vec<u8>, Box<dyn Error>> {

        // Handle removal of resources claimed by an actor here
        Ok(vec![])
    }

    fn get_boot_time(
        &self,
        _actor_id: &str,
        _req: SysInfoRequest,
    ) -> Result<Vec<u8>, Box<dyn Error>> {
        let boot_time = sys_info::boottime().unwrap();
        let boottime_str = format!("{} sec, {} usec", boot_time.tv_sec, boot_time.tv_usec);
        
        let output_sysinfo = SysInfoRequest {
            body: boottime_str.clone(),
        };
        Ok(serialize(output_sysinfo)?)
    } 

    fn get_cpu_num(
        &self,
        _actor_id: &str,
        _req: SysInfoRequest) -> Result<Vec<u8>, Box<dyn Error>> {
        let cpu_num = sys_info::cpu_num();
        let cpu_num_str = format!("{}", cpu_num.unwrap());
        
        let output_sysinfo = SysInfoRequest {
            body: cpu_num_str.clone(),
        };
        Ok(serialize(output_sysinfo)?)
    } 
    
    fn get_cpu_speed(
        &self,
        _actor_id: &str,
        _req: SysInfoRequest) -> Result<Vec<u8>, Box<dyn Error>> {
        let cpu_speed = sys_info::cpu_speed();
        let cpu_speed_str = format!("{}", cpu_speed.unwrap());
        
        let output_sysinfo = SysInfoRequest {
            body: cpu_speed_str.clone(),
        };
        Ok(serialize(output_sysinfo)?)
    } 
    
    fn get_disk_info(
        &self,
        _actor_id: &str,
        _req: SysInfoRequest) -> Result<Vec<u8>, Box<dyn Error>> {
        let disk_info = sys_info::disk_info().unwrap();
        let disk_info_str = format!("total {} KB, free {} KB", disk_info.total, disk_info.free);

        let output_sysinfo = SysInfoRequest {
            body: disk_info_str.clone(),
        };
        Ok(serialize(output_sysinfo)?)
    } 
    
    fn get_load_avg(
        &self,
        _actor_id: &str,
        _req: SysInfoRequest) -> Result<Vec<u8>, Box<dyn Error>> {
        let load = sys_info::loadavg().unwrap();
        let load_str = format!("{} {} {}", load.one, load.five, load.fifteen);

        let output_sysinfo = SysInfoRequest {
            body: load_str.clone(),
        };
        Ok(serialize(output_sysinfo)?)
    } 
    
    fn get_mem_info(
        &self,
        _actor_id: &str,
        _req: SysInfoRequest) -> Result<Vec<u8>, Box<dyn Error>> {
        let mem = sys_info::mem_info().unwrap();
        let mem_info_str = format!("total {} KB, free {} KB, avail {} KB, buffers {} KB, cached {} KB",
            mem.total, mem.free, mem.avail, mem.buffers, mem.cached);

        let output_sysinfo = SysInfoRequest {
            body: mem_info_str.clone(),
        };
        Ok(serialize(output_sysinfo)?)
    } 
    
    fn get_proc_total(
        &self,
        _actor_id: &str,
        _req: SysInfoRequest,
    ) -> Result<Vec<u8>, Box<dyn Error>> {
        let proc_total = sys_info::proc_total();
        let proc_str = format!("{}", proc_total.unwrap());

        let output_sysinfo = SysInfoRequest {
            body: proc_str.clone(),
        };
        Ok(serialize(output_sysinfo)?)
    }
    
}

impl CapabilityProvider for SysinfoProvider {
    fn capability_id(&self) -> &'static str {
        CAPABILITY_ID
    }

    // Invoked by the runtime host to give this provider plugin the ability to communicate
    // with actors
    fn configure_dispatch(&self, dispatcher: Box<dyn Dispatcher>) -> Result<(), Box<dyn Error>> {
        trace!("Dispatcher received.");
        let mut lock = self.dispatcher.write().unwrap();
        *lock = dispatcher;

        Ok(())
    }

    fn name(&self) -> &'static str {
        "New SysinfoProvider Capability Provider" // TODO: change this friendly name
    }

    // Invoked by host runtime to allow an actor to make use of the capability
    // All providers MUST handle the "configure" message, even if no work will be done
    fn handle_call(&self, actor: &str, op: &str, msg: &[u8]) -> Result<Vec<u8>, Box<dyn Error>> {
        trace!("Received host call from {}, operation - {}", actor, op);

        match op {            
            OP_BIND_ACTOR if actor == SYSTEM_ACTOR => self.configure(deserialize(msg)?),
            OP_REMOVE_ACTOR if actor == SYSTEM_ACTOR => self.deconfigure(deserialize(msg)?),
            sysinfo::OP_GET_BOOT_TIME => self.get_boot_time(actor, deserialize(msg)?),
            sysinfo::OP_GET_NCPU => self.get_cpu_num(actor, deserialize(msg)?),
            sysinfo::OP_GET_CPU_SPEED => self.get_cpu_speed(actor, deserialize(msg)?),
            sysinfo::OP_GET_DISK_INFO => self.get_disk_info(actor, deserialize(msg)?),
            sysinfo::OP_GET_LOAD_AVG => self.get_load_avg(actor, deserialize(msg)?),
            sysinfo::OP_GET_MEM_INFO => self.get_mem_info(actor, deserialize(msg)?),
            sysinfo::OP_GET_PROC_TOTAL => self.get_proc_total(actor, deserialize(msg)?),

            _ => Err("bad dispatch".into()),
        }
    }
}
